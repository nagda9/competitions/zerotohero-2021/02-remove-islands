package com.zth;

import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

// Checks for 1s in a Point's neighborhood on a new Thread
public class PointSearcher extends Thread{
    private final Point point;
    private final AtomicInteger underProcessing;
    private final Stack<Point> points;
    private final int[][] initialMatrix;
    private final int[][] output;

    private final int[][] directions = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

    PointSearcher(Point point, AtomicInteger underProcessing, Stack<Point> points, int[][] initialMatrix, int[][] output) {
        this.point = point;
        this.underProcessing = underProcessing;
        this.points = points;
        this.initialMatrix = initialMatrix;
        this.output = output;
    }

    @Override
    public void run() {
        // Check neighbors of a Point in 4 directions: up, down, left, right
        for (var dir: directions) {
            Point p = new Point(point.i+dir[0], point.j+dir[1]);
            // Check if the neighbor is inside the matrix
            if (p.i >= 0 && p.i < initialMatrix.length && p.j >= 0 && p.j < initialMatrix[0].length) {
                // Check if Point's value in the initialMatrix is 1, and in the output is 0, so it was not already examined
                if (initialMatrix[p.i][p.j] == 1 && output[p.i][p.j] == 0) {
                    // update the output matrix, and add the point to the stack for processing
                    output[p.i][p.j] = 1;
                    synchronized (points) {
                        points.push(p);
                    }
                }
            }
        }
        // Point processed, processing counter can be decremented
        synchronized (underProcessing) {
            underProcessing.decrementAndGet();
        }
    }
}
