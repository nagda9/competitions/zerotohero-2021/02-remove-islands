package com.zth;

import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class IslandRemover {
    public final int[][] initialMatrix;
    private final Stack<Point> points; // stack for Points to be processed in parallel
    private final AtomicInteger underProcessing; // a counter to prevent exiting while cycle before every Point has been processed

    public IslandRemover(int[][] initialMatrix) {
        this.initialMatrix = initialMatrix;
        this.points = getBorderElements();
        this.underProcessing = new AtomicInteger(0);
    }

    // Collecting the coordinates of points from the border of the matrix where the value is 1
    public Stack<Point> getBorderElements() {
        var points = new Stack<Point>();

        for (int i = 0; i < initialMatrix.length; ++i) {
            // Collecting border points from the first column
            if (initialMatrix[i][0] == 1) {
                Point point = new Point(i, 0);
                points.push(point);
            }

            // Collecting border points from the last column
            if (initialMatrix[i][initialMatrix[i].length-1] == 1) {
                Point point = new Point(i, initialMatrix[i].length-1);
                points.push(point);
            }

            // Collecting border points from the first and last row
            if (i == 0 || i == initialMatrix.length-1) {
                for (int j = 1; j < initialMatrix[i].length-1; ++j) {
                    if (initialMatrix[i][j] == 1) {
                        Point point = new Point(i, j);
                        points.push(point);
                    }
                }
            }
        }
        return points;
    }

    // Function to remove islands of 1s from a matrix
    public int[][] removeIslands() {
        // Generate nullmatrix
        int[][] output = new int[initialMatrix.length][initialMatrix[0].length];

        // Fill up the output matrix with border points
        for (var point: points) {
            output[point.i][point.j] = 1;
        }

        // Search for new points attached to the border with parallel processing
        ExecutorService threadHandler = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        while(true) {
            synchronized (points) {
                // the stack is not empty, new threads can be started to find new Points
                if (!points.isEmpty()) {
                    // Increment underProcessing counter for each Thread started, and process the top Point of the stack
                    synchronized (underProcessing) {
                        underProcessing.incrementAndGet();
                        Point point = points.pop();
                        threadHandler.execute(new PointSearcher(point, underProcessing, points, initialMatrix, output));
                    }
                } else {
                    // if there are no more points in the stack, and there are no more points under processing, then
                    // the cycle can be exited, and the threadHeandler can be stopped
                    synchronized (underProcessing) {
                        if (underProcessing.intValue() == 0) {
                            break;
                        }
                    }
                }
            }
        }
        threadHandler.shutdown();

        return output;
    }
}
