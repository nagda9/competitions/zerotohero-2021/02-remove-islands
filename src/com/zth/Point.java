package com.zth;

// Point class for storing coordinates of a matrix
public class Point implements Comparable<Point>{
    public int i; // row coord of matrix
    public int j; // column coord of matrix

    // Method for printing data
    @Override
    public String toString() {
        return ("[" + i + ", " + j + "]");
    }

    Point(int i, int j) {
        this.i = i;
        this.j = j;
    }

    // Comparing Point types (for sorting)
    @Override
    public int compareTo(Point o) {
        if (i == o.i && j == o.j) { // i and j equals
            return 0;
        } else if (i != o.i) { // i is not equal
            if (i > o.i) {
                return 1; // this is greater than o
            } else {
                return -1; // this is smaller than o
            }
        } else { // i is equal but j is not
            if (j > o.j) {
                return 1; // this is greater than o
            } else {
                return -1;
            }
        }
    }
}
