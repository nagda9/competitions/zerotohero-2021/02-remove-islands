package com.zth;

import java.util.Arrays;
import java.util.Stack;

// Main class for testing
public class Main {
    // simple test cases
    private static final int[][] first = {
            {0,0,0},
            {0,1,0},
            {0,0,0}
    };
    private static final int[][] firstSolution = {
            {0,0,0},
            {0,0,0},
            {0,0,0}
    };

    private static final int[][] second = {
            {0,0,1,0,0},
            {0,0,1,0,0},
            {0,0,1,1,0},
            {0,0,1,0,0},
            {0,0,0,0,0}
    };
    private static final int[][] third = {
            {1,0,0,0,0,0},
            {0,1,0,1,1,1},
            {0,0,1,0,1,0},
            {1,1,0,0,1,0},
            {1,0,1,1,0,0},
            {1,0,0,0,0,1}
    };
    private static final int[][] thirdSolution = {
            {1,0,0,0,0,0},
            {0,0,0,1,1,1},
            {0,0,0,0,1,0},
            {1,1,0,0,1,0},
            {1,0,0,0,0,0},
            {1,0,0,0,0,1}
    };

    // text formatting variables
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_BLUE = "\u001B[34m";

    // Unit test for IslandRemover.getBorderElements()
    private static void checkBorder (Stack<Point> result, int[][] initialMatrix) {
        Stack<Point> resultCopy = (Stack<Point>)result.clone();
        boolean isOK = true;
        Point wrong = null;
        while(!resultCopy.isEmpty()) {
            Point point = resultCopy.pop();
            if (initialMatrix[point.i][point.j] != 1) {
                isOK = false;
                wrong = point;
                break;
            }
        }

        if (isOK) {
            System.out.println(ANSI_GREEN + "border PASSED" + ANSI_RESET);
        } else {
            System.out.println(ANSI_RED + "border FAILED at point: " + wrong + ANSI_RESET);
        }
        System.out.println(Arrays.deepToString(result.toArray()));
    }

    // Unit test for IslandRemover.removeIslands()
    private static void checkResult (int[][] result, int[][] solution) {
        if (Arrays.deepEquals(result, solution)) {
            System.out.println(ANSI_GREEN + "result PASSED" + ANSI_RESET);
        } else {
            System.out.println(ANSI_RED + "result FAILED" + ANSI_RESET);
            System.out.println("Your result is:");
            System.out.println(Arrays.deepToString(result).replace("],", "],\n"));
            System.out.println("The correct solution is:");
        }
        System.out.println(Arrays.deepToString(solution).replace("],", "],\n"));
    }

    // testing
    public static void main(String[] args) {
        // generating random test matrix
        int[][] testRandom = new int[100][100];
        for(int i = 0; i < testRandom.length; ++i){
            for(int j = 0; j < testRandom[0].length; ++j){
                if (Math.random() > 0.7) {
                    testRandom[i][j] = 1;
                }
            }
        }

        // Creating an array from the test cases
        IslandRemover[] islandRemovers = {
                new IslandRemover(first),
                new IslandRemover(second),
                new IslandRemover(third),
                new IslandRemover(testRandom)
        };

        // Creating an array from the expected solutions (where it is available)
        int[][][] removeIslandSolutions = {firstSolution, second, thirdSolution};

        // Loop through the test cases
        for (int i = 0; i < islandRemovers.length; ++i) {
            System.out.println(ANSI_BLUE + i + ". example" + ANSI_RESET);
            System.out.println(Arrays.deepToString(islandRemovers[i].initialMatrix).replace("],", "],\n"));
            checkBorder(islandRemovers[i].getBorderElements(), islandRemovers[i].initialMatrix);

            long startTime = System.currentTimeMillis();
            if (removeIslandSolutions.length > i) {
                checkResult(islandRemovers[i].removeIslands(), removeIslandSolutions[i]);
            } else {
                System.out.println("result not checked");
                System.out.println(Arrays.deepToString(islandRemovers[i].removeIslands()).replace("],", "],\n"));
            }
            long estimatedTime = System.currentTimeMillis() - startTime;
            System.out.println("Runtime: " + estimatedTime);
            System.out.println();
        }
    }
}
